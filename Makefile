# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rvievill <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/07/14 11:57:02 by rvievill          #+#    #+#              #
#    Updated: 2016/08/30 17:51:19 by rvievill         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = minishell

SRC_NAMES =	main.c \
			env.c \
			cd.c \
			free_tab.c \
			free_list.c \
			create_lst_path.c \
			minishell.c \
			util_lst.c \
			lexer.c \
			double_quote.c \
			cmd_arg.c \
			echo.c \
			unsetenv.c \
			utils_builtin.c \
			parser.c \
			exec_builtin.c \
			exit.c \
			setenv.c \
			unsetenv.c \
			cmd_exist.c \
			exec_cmd.c \
			check_arg.c \
			check_var.c \
			exec_spe.c \
			print_shell.c \
			env2.c \
			error_cd.c

OBJ_NAMES = $(SRC_NAMES:.c=.o)
INC_NAMES = libft.h
LIB_NAMES = libft.a

SRC_PATH = ./src
OBJ_PATH = ./obj
INC_PATH = ./incude ./libft
LIB_PATH = ./libft/

SRC = $(addprefix $(SRC_PATH)/,$(SRC_NAMES))
OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_NAMES))
INC = $(addprefix -I,$(INC_PATH))
LIB = $(LIB_PATH)$(LIB_NAMES)

################################################################################

FLAGS = -Wall -Wextra -Werror
LDLIBS = -lft
COM = gcc #-fsanitize=address

################################################################################

all: $(LIB) $(NAME)

$(NAME): $(LIB) $(OBJ)
	@make -C libft
	$(COM) $^ -o $@ -L $(LIB_PATH) -lft

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || true
	$(COM) $(FLAGS) $(INC) -o $@ -c $<

$(LIB):
	@make -C $(LIB_PATH)

clean:
	@make clean -C libft
	@rm -rf $(OBJ_PATH) 2> /dev/null || true
	@rmdir $(OBJ_PATH) obj 2> /dev/null || echo  > /dev/null
	@rm -f $(OBJ)

fclean: clean
	@make fclean -C libft
	@rm -rf $(NAME)

re: clean fclean all

.PHONY: all clean fclean re
