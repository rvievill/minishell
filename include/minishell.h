/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 16:51:14 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/25 14:45:42 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include <dirent.h>
# include <sys/stat.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <sys/wait.h>
# include <signal.h>
# include "../libft/libft.h"

# define RED "\033[3;31m"
# define GREEN "\033[3;32m"
# define DEF "\033[0m"

typedef struct		s_env
{
	char			*var;
	char			*content;
	struct s_env	*next;
	struct s_env	*prev;
}					t_env;

typedef struct		s_cmd
{
	char			*cmd;
	char			**arg;
	char			*sep;
	struct s_cmd	*next;
	struct s_cmd	*prev;
}					t_cmd;

void				modify_env(t_env *start, char **arg);
void				cpy_env(t_env **start, t_env *env);

/*
** print_shell.c
*/
void				print_shell(t_env *env);

/*
** cmd_arg.c
*/
void				add_cmd(char **line, char *save, int i, t_cmd **cur);
void				add_arg(char **line, t_cmd **cur, int k);

/*
** double_quote.c
*/
int					is_quote(char *line);
void				dquote(char **line, char **cmd);

/*
** env.c
*/
int					ft_env(t_cmd *cmd, t_env **env);

/*
** free_list.c
*/
void				free_node(t_cmd *start);
void				free_env(t_env **start);
void				free_cmd(t_cmd **start);

/*
** free_tab.c
*/
void				free_tab(char **tab);

/*
** lexer.c
*/
t_cmd				*lexer(char *line, t_env **env);
int					is_sep(char *line);

/*
** minishell.c
*/
void				minishell(t_env **env);

/*
** utils_lst.c
*/
void				add_elem(t_cmd **cur);
void				init_node(t_cmd **elem);
void				up_list(t_cmd **cur);

/*
** create_lst_path.c
*/
void				create_lst(t_env **env, char **envp);

/*
** unsetenv.c
*/
int					ft_unsetenv(char *name, t_env **env);

/*
** setenv.c
*/
int					ft_setenv(char **arg, t_env **env);

/*
** echo.c
*/
int					ft_echo(char **arg, t_env **env);

/*
** cd.c
*/
int					ft_cd(char **arg, t_env **env);

/*
** exit.c
*/
int					ft_exit(t_cmd *cmd, t_env *env);

/*
** utils_builtin.c
*/
char				*recovery_content(char *var, t_env **env);
void				change_env(char *new_env, t_env **env);

/*
** parser.c
*/
void				parser(t_cmd **cmd, t_env **env, char **path);

/*
** exec_builtin.c
*/
int					exec_builtin(t_cmd *cmd, t_env **env);

/*
** cmd_exist.c
*/
int					is_builtin(char *cmd);
int					is_no_builtin(char **cmd, char **path);

/*
** exec_cmd.c
*/
int					exec_cmd(t_cmd *cmd, t_env **env);

/*
** check_arg.c
*/
void				check_arg(t_cmd *cmd);

/*
** check_var.c
*/
void				check_var(t_cmd *cmd, t_env *env);

/*
** exec_spe.c
*/
void				exec_spe(t_cmd *cmd);

/*
** error_cd.c
*/
int					error_cd(char *dir, int nb);

#endif
