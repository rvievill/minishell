/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cpyav.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/09 16:15:58 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/12 14:46:28 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			**ft_tabcpy(char **av)
{
	char		**str;
	int			i;
	int			size;

	i = 0;
	size = 0;
	while (av && av[size])
		size++;
	str = (char **)malloc(sizeof(char *) * (size + 1));
	if (size == 0)
	{
		str[0] = ft_strdup("");
		str[1] = NULL;
		return (str);
	}
	while (i < size && av[i])
	{
		str[i] = ft_strdup(av[i]);
		i++;
	}
	str[i] = NULL;
	return (str);
}
