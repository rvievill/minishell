/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/12 15:26:57 by rvievill          #+#    #+#             */
/*   Updated: 2016/07/17 13:38:16 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char			*ft_strndup(char *s1, int n)
{
	size_t		len;
	char		*tab;

	len = ft_strlen(s1);
	if ((size_t)n < len)
		len = n;
	if (!(tab = (char *)malloc(sizeof(char) * (len + 1))))
		return (0);
	tab[len] = '\0';
	return (ft_memcpy(tab, s1, len));
}
