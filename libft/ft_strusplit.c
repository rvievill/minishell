/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strusplit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/14 11:53:35 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/14 12:02:50 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strusplit(char **tab, char *c)
{
	char	*line;

	line = ft_strdup("");
	while (*tab)
	{
		line = ft_strjoin(line, *tab);
		line = ft_strjoin(line, c);
		tab++;
	}
	return (line);
}
