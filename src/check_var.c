/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_var.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/03 16:44:33 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/30 17:51:04 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"
#include <stdio.h>

static int		size_var(char *str, t_env *env)
{
	while (env)
	{
		if (str && ft_strcmp(env->content, str) == 0)
			return (ft_strlen(env->var) - 1);
		env = env->next;
	}
	return (0);
}

static char		*new_str(char *arg, t_env *env)
{
	char		*new;
	int			i;
	char		*content;
	int			j;

	i = 0;
	j = 0;
	while (arg[i] != '$')
		i++;
	new = ft_strndup(arg, i);
	if ((content = recovery_content(&arg[i], &env)))
		new = ft_strjoin(new, content);
	while (arg[i] && j++ <= size_var(content, env))
		i++;
	if (arg[i] && arg[i] != '$' && arg[i] != ' ' && arg[i] != '\t')
	{
		while (arg[i] && arg[i] != ' ' && arg[i] != '\t' && arg[i] != '$')
			i++;
	}
	arg = &arg[i];
	new = ft_strjoin(new, arg);
	return (new);
}

void			check_var(t_cmd *cmd, t_env *env)
{
	t_cmd		*tmp;
	int			i;
	char		*save;
	char		*s;

	tmp = cmd;
	while (tmp)
	{
		i = 0;
		while (tmp->arg && tmp->arg[++i])
		{
			while ((s = ft_strchr(tmp->arg[i], '$')) && *(s + 1) != ' '
					&& *(s + 1) != '\0' && *s != '\0')
			{
				save = ft_strdup(tmp->arg[i]);
				free(tmp->arg[i]);
				tmp->arg[i] = new_str(save, env);
				free(save);
				s = NULL;
			}
		}
		tmp = tmp->next;
	}
}
