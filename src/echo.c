/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 14:47:47 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/19 14:19:29 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static void			put_arg(char *arg)
{
	int				i;

	i = 0;
	while (*arg)
	{
		i = 1;
		if (*arg == '"')
			arg++;
		ft_putchar(*arg);
		arg++;
	}
	if (i == 1)
		ft_putchar(' ');
}

int					ft_echo(char **arg, t_env **env)
{
	int				i;

	(void)env;
	i = 1;
	if (arg[1] && ft_strcmp(arg[1], "-n") == 0)
		i++;
	while (arg[i])
	{
		put_arg(arg[i]);
		if (arg[i] == '\0')
			i++;
		i++;
	}
	if (arg[1] == NULL || ft_strcmp(arg[1], "-n") != 0)
		ft_putchar('\n');
	return (0);
}
