/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_cmd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/25 14:15:54 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/22 15:32:44 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"
#include <sys/wait.h>

static int		size_lst(t_env **env)
{
	int			i;
	t_env		*start;

	i = 0;
	if (env)
		start = *env;
	while (env && *env)
	{
		i++;
		*env = (*env)->next;
	}
	if (env)
		*env = start;
	return (i);
}

static char		**struct_to_tab(t_env **env)
{
	char		**tab;
	int			i;
	int			size;
	char		*tmp;
	t_env		*start;

	i = 0;
	tmp = NULL;
	if (env)
		start = *env;
	size = size_lst(env);
	if (!(tab = (char **)ft_memalloc(sizeof(char *) * (size + 1))))
		return (NULL);
	while (env && *env)
	{
		tmp = ft_strdup((*env)->var);
		tab[i] = ft_strjoin(tmp, (*env)->content);
		i++;
		*env = (*env)->next;
	}
	if (env)
		*env = start;
	tab[i] = NULL;
	return (tab);
}

static void		change_shlvl(t_env **env, int nb)
{
	char		*new_nb;

	while (env && ft_strcmp((*env)->var, "SHLVL="))
		*env = (*env)->next;
	new_nb = ft_itoa(ft_atoi(((*env)->content)) + nb);
	free((*env)->content);
	(*env)->content = ft_strdup(new_nb);
	free(new_nb);
	while ((*env)->prev)
		*env = (*env)->prev;
}

int				exec_cmd(t_cmd *cmd, t_env **env)
{
	int			status;
	pid_t		father;
	char		**tab;

	status = 0;
	if (env && ft_strstr(cmd->cmd, "./minishell"))
		change_shlvl(env, 1);
	tab = struct_to_tab(env);
	father = fork();
	if (father != 0)
		waitpid(father, &status, 0);
	else
		execve(cmd->cmd, cmd->arg, tab);
	if (env && ft_strstr(cmd->cmd, "./minishell"))
		change_shlvl(env, -1);
	free_tab(tab);
	return (WEXITSTATUS(status));
}
