/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_lst_path.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/20 15:25:03 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/19 15:29:28 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static void		inisialize_lst(t_env *env)
{
	env->var = NULL;
	env->content = NULL;
	env->next = NULL;
	env->prev = NULL;
}

static void		filling_link(t_env **new, char *var)
{
	int			i;
	char		*name;
	char		*content;

	i = 0;
	while (var && var[i] != '=')
		i++;
	i++;
	name = ft_strndup(var, i);
	(*new)->var = ft_strdup(name);
	free(name);
	content = ft_strdup(var + i);
	(*new)->content = ft_strdup(content);
	free(content);
	(*new)->next = NULL;
}

static void		create_link(t_env **start, char *var)
{
	t_env		*new;

	if (!(new = (t_env *)ft_memalloc(sizeof(t_env))))
		return ;
	if (new)
	{
		inisialize_lst(new);
		filling_link(&new, var);
		while (*start && (*start)->next)
			*start = (*start)->next;
		new->prev = *start;
		(*start)->next = new;
	}
}

void			create_lst(t_env **env, char **envp)
{
	int			i;
	t_env		*begin;

	i = 1;
	if (!(*env = (t_env*)ft_memalloc(sizeof(t_env))))
		return ;
	inisialize_lst(*env);
	filling_link(env, envp[0]);
	begin = *env;
	while (envp[i] != NULL)
	{
		create_link(env, envp[i]);
		i++;
	}
	*env = begin;
}
