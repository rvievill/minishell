/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 17:19:03 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/15 12:08:29 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

int			ft_exit(t_cmd *cmd, t_env *env)
{
	int		nb;

	if (cmd->arg[1] && cmd->arg[2])
	{
		ft_putstr_fd("exit: too many arguments\n", 2);
		return (1);
	}
	if (cmd->arg[1])
	{
		nb = ft_atoi(cmd->arg[1]);
		free_cmd(&cmd);
		free_env(&env);
		exit(nb);
	}
	free_cmd(&cmd);
	free_env(&env);
	exit(0);
}
