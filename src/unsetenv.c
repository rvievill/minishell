/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unsetenv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 17:18:31 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/22 10:59:33 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static void		free_maillon(t_env **maillon)
{
	ft_bzero((*maillon)->var, ft_strlen((*maillon)->var));
	ft_bzero((*maillon)->content, ft_strlen((*maillon)->content));
	free((*maillon)->var);
	free((*maillon)->content);
	free(*maillon);
}

static int		fix_maillon(t_env **env, char *name)
{
	if (env && ft_strncmp(name, (*env)->var, ft_strlen((*env)->var) - 1) == 0)
	{
		if ((*env)->prev)
			(*env)->prev->next = (*env)->next;
		if ((*env)->next)
			(*env)->next->prev = (*env)->prev;
		free_maillon(env);
		if ((*env)->next)
			*env = (*env)->next;
		else
			*env = (*env)->prev;
		while ((*env)->prev)
			*env = (*env)->prev;
		return (0);
	}
	return (1);
}

int				ft_unsetenv(char *name, t_env **env)
{
	char		*maj_name;
	t_env		*start;

	if (env)
		start = *env;
	maj_name = ft_strupcase(name);
	while (env && (*env)->next)
	{
		if (fix_maillon(env, maj_name) == 0)
		{
			if (env)
				*env = start;
			return (0);
		}
		*env = (*env)->next;
	}
	if (fix_maillon(env, maj_name) == 0)
	{
		if (env)
			*env = start;
		return (0);
	}
	if (env)
		*env = start;
	return (-1);
}
