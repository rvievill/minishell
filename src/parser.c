/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/09 19:24:55 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/25 15:13:31 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"
#include <stdio.h>

static void			error_cmd(t_cmd **cmd, int *ret)
{
	ft_putstr_fd("MyShell: command not found: ", 2);
	ft_putstr_fd((*cmd)->cmd, 2);
	ft_putchar_fd('\n', 2);
	*ret = -1;
}

static void			progress_cmd(int ret, t_cmd **cmd)
{
	if (ret == 0)
	{
		while ((*cmd)->next && ft_strcmp((*cmd)->sep, "||") == 0)
		{
			*cmd = (*cmd)->next;
			if ((*cmd)->prev)
				free_node((*cmd)->prev);
		}
	}
	else
	{
		while ((*cmd)->next && ft_strcmp((*cmd)->sep, "&&") == 0)
		{
			*cmd = (*cmd)->next;
			if ((*cmd)->prev)
				free_node((*cmd)->prev);
		}
	}
}

static void			push_and_free(t_cmd **cmd, t_cmd **cur)
{
	*cmd = *cur;
	*cur = (*cur)->next;
	free_node(*cmd);
	*cmd = *cur;
}

void				parser(t_cmd **cmd, t_env **env, char **path)
{
	t_cmd			*cur;
	int				ret;

	ret = 0;
	cur = *cmd;
	while (cur && cur->cmd)
	{
		if (is_builtin(cur->cmd) == 0)
			ret = exec_builtin(*cmd, env);
		else if (is_no_builtin(&(*cmd)->cmd, path) == 0)
		{
			if (env)
				ret = exec_cmd(*cmd, env);
			else
				ret = exec_cmd(*cmd, NULL);
		}
		else
			error_cmd(&cur, &ret);
		progress_cmd(ret, &cur);
		if (cur)
			push_and_free(cmd, &cur);
	}
	if (cur)
		free_node(*cmd);
}
