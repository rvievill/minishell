/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/12 16:01:08 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/30 17:50:18 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static char			**ft_cmd(char **arg, char **path)
{
	int				i;

	i = 1;
	while (arg[i])
	{
		if (ft_strstr(arg[i], "./"))
			return (&(arg[i]));
		if (is_builtin(arg[i]) == 0)
			return (&arg[i]);
		else if (is_no_builtin(&arg[i], path) == 0)
			return (&arg[i]);
		i++;
	}
	return (NULL);
}

static void			display(t_env *start)
{
	t_env			*tmp;

	tmp = start;
	while (tmp)
	{
		ft_putstr(tmp->var);
		ft_putendl(tmp->content);
		tmp = tmp->next;
	}
	free_env(&start);
}

static void			display_env(t_env **env, char **arg)
{
	t_env		*start;
	int			i;

	i = 0;
	if (!env)
	{
		while (arg[i])
		{
			if (ft_strchr(arg[i], '='))
				ft_putendl(arg[i]);
			i++;
		}
		return ;
	}
	start = NULL;
	cpy_env(&start, *env);
	modify_env(start, arg);
	display(start);
}

static void			exec_new_cmd(char **cmd, t_env **env, char **path, int opt)
{
	char			*line;
	t_cmd			*new_cmd;
	int				i;

	i = 1;
	if (opt == 1)
		i = 2;
	line = ft_strusplit(&cmd[0], " ");
	new_cmd = lexer(line, env);
	opt == 0 ? parser(&new_cmd, env, path) : parser(&new_cmd, NULL, path);
	free(line);
}

int					ft_env(t_cmd *cmd, t_env **env)
{
	char			**path;
	int				opt;
	char			**str;

	opt = 0;
	if (cmd->arg[1] && ft_strcmp(cmd->arg[1], "-i") == 0)
		opt = 1;
	path = ft_strsplit(recovery_content("$PATH", env), ':');
	if (cmd->arg[1] && (str = ft_cmd(cmd->arg, path)))
	{
		if (ft_strcmp(str[0], "exit") == 0)
		{
			ft_putendl_fd("env: exit: No such file or directory", 2);
			return (1);
		}
		if (ft_strcmp(str[0], "env") == 0)
			display_env(env, cmd->arg);
		else
			exec_new_cmd(str, env, path, opt);
	}
	else
		opt == 0 ? display_env(env, cmd->arg) : display_env(NULL, cmd->arg);
	free_tab(path);
	return (0);
}
