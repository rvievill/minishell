/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_builtin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/24 16:59:11 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/22 16:25:18 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

int			exec_builtin(t_cmd *cmd, t_env **env)
{
	int		ret;

	ret = 0;
	if (ft_strcmp(cmd->cmd, "env") == 0)
		ret = ft_env(cmd, env);
	if (ft_strcmp(cmd->cmd, "setenv") == 0)
		ret = ft_setenv(cmd->arg, env);
	if (ft_strcmp(cmd->cmd, "unsetenv") == 0)
		ret = ft_unsetenv(cmd->arg[1], env);
	if (ft_strcmp(cmd->cmd, "echo") == 0)
		ret = ft_echo(cmd->arg, env);
	if (ft_strcmp(cmd->cmd, "cd") == 0)
		ret = ft_cd(cmd->arg, env);
	if (ft_strcmp(cmd->cmd, "exit") == 0)
		ret = ft_exit(cmd, *env);
	return (ret);
}
