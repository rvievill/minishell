/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env2.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 18:04:20 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/30 17:50:14 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"
#include <stdio.h>

static void		fill_lst(t_env *new, t_env *env)
{
	new->var = ft_strdup(env->var);
	new->content = ft_strdup(env->content);
	new->next = NULL;
}

void			cpy_env(t_env **start, t_env *env)
{
	t_env		*new;
	t_env		*begin;

	*start = (t_env *)malloc(sizeof(t_env));
	fill_lst(*start, env);
	env = env->next;
	begin = *start;
	while (env)
	{
		*start = begin;
		new = (t_env *)malloc(sizeof(t_env));
		fill_lst(new, env);
		while ((*start)->next)
			*start = (*start)->next;
		(*start)->next = new;
		env = env->next;
	}
	*start = begin;
}

static void		add_end_lst(t_env *start, char *arg)
{
	t_env		*new;
	int			i;

	i = 0;
	if (!(new = (t_env *)malloc(sizeof(t_env))))
		return ;
	while (arg[i] && arg[i] != '=')
		i++;
	if (arg[i] == '\0')
	{
		free(new);
		return ;
	}
	new->var = ft_strndup(arg, i + 1);
	new->content = ft_strdup(&arg[i + 1]);
	new->next = NULL;
	while (start->next)
		start = start->next;
	start->next = new;
}

static void		check(t_env *start, int i, int j, char **arg)
{
	while (start)
	{
		j = 0;
		if (ft_strncmp(start->var, arg[i], ft_strlen(start->var)) == 0)
		{
			while (arg[i][j] != '=')
				j++;
			free(start->content);
			start->content = ft_strdup(&arg[i][++j]);
			ft_bzero(arg[i], ft_strlen(arg[i]));
		}
		start = start->next;
	}
}

void			modify_env(t_env *start, char **arg)
{
	int			i;
	t_env		*begin;
	int			j;

	begin = start;
	i = 0;
	while (arg[++i])
	{
		j = 0;
		check(start, i, j, arg);
		start = begin;
		add_end_lst(start, arg[i]);
		start = begin;
	}
	start = begin;
}
