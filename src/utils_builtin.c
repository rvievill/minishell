/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_builtin.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/11 16:10:03 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/22 16:25:10 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

char			*recovery_content(char *var, t_env **env)
{
	t_env		*start;
	char		*save;

	var++;
	start = NULL;
	if (env)
		start = *env;
	while (env && *env)
	{
		if (ft_strncmp(var, (*env)->var, ft_strlen((*env)->var) - 1) == 0)
		{
			save = (*env)->content;
			*env = start;
			return (save);
		}
		*env = (*env)->next;
	}
	if (env)
		*env = start;
	return (NULL);
}

void			change_env(char *new_env, t_env **env)
{
	char		*save;
	t_env		*start;

	if (!env)
		return ;
	start = *env;
	save = ft_strdup(recovery_content("$PWD=", env));
	while (*env && ft_strcmp("PWD=", (*env)->var) != 0)
		*env = (*env)->next;
	free((*env)->content);
	(*env)->content = ft_strdup(new_env);
	*env = start;
	while (*env && ft_strcmp("OLDPWD=", (*env)->var) != 0)
		*env = (*env)->next;
	free((*env)->content);
	(*env)->content = ft_strdup(save);
	*env = start;
	free(save);
}
