/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_spe.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/16 17:51:44 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/25 17:44:31 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"
#include <stdio.h>

static void		change(char **arg)
{
	char		*save;
	char		*path;
	int			i;

	i = 0;
	save = NULL;
	path = NULL;
	while ((*arg)[i] && (*arg)[i] != '/' && (*arg)[i - 1] != '.')
		i++;
	if (i != 0)
		path = ft_strndup(*arg, ++i);
	save = getcwd(save, sizeof(save));
	save = ft_strjoin(save, "/");
	save = ft_strjoin(save, path);
	free(path);
	path = ft_strjoin(save, &((*arg)[i]));
	if (*arg)
		free(*arg);
	*arg = ft_strdup(path);
	free(path);
}

void			exec_spe(t_cmd *cmd)
{
	t_cmd		*start;

	start = cmd;
	while (start)
	{
		if (start->cmd && ft_strstr(start->cmd, "./"))
			change(&start->cmd);
		start = start->next;
	}
}
