/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util_lst.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/24 16:36:38 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/30 15:14:04 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

void			up_list(t_cmd **cur)
{
	t_cmd		*tmp;

	tmp = *cur;
	while (tmp->prev != NULL)
		tmp = tmp->prev;
	*cur = tmp;
}

void			init_node(t_cmd **elem)
{
	(*elem)->cmd = NULL;
	(*elem)->arg = NULL;
	(*elem)->sep = NULL;
	(*elem)->next = NULL;
	(*elem)->prev = NULL;
}

void			add_elem(t_cmd **cur)
{
	t_cmd		*new;

	if ((new = (t_cmd *)malloc(sizeof(t_cmd))))
	{
		init_node(&new);
		new->prev = *cur;
		while ((*cur)->next)
			*cur = (*cur)->next;
		(*cur)->next = new;
		*cur = new;
	}
}
