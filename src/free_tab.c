/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_tab.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/19 17:21:53 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/19 12:28:38 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static int		size_tab(char **tab)
{
	char		**save;

	save = tab;
	while (tab && *tab)
		tab++;
	return (tab - save);
}

void			free_tab(char **tab)
{
	int			i;
	int			size;

	size = size_tab(tab);
	i = 0;
	while (tab && i < size)
	{
		free(tab[i]);
		i++;
	}
	free(tab);
}
