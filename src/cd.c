/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 17:17:29 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/30 17:31:29 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static void				print_message(struct stat info, int nb, t_env **env,
										char *arg)
{
	if (!env)
	{
		ft_putstr("/usr/bin/cd: line 4: cd: ");
		ft_putstr(arg);
		if (arg && S_ISDIR(info.st_mode) == 0)
			ft_putendl("No such file or directory");
		else if (!arg && S_ISDIR(info.st_mode) == 0)
			ft_putendl("HOME not set");
		else
			ft_putendl("Not a directory");
	}
	if (env && nb == -1 && S_ISLNK(info.st_mode) != 0)
		ft_putstr_fd("cd: too many levels of symbolic links: ", 2);
	else if (env && nb == -1)
		ft_putstr_fd("cd: no such file or directory: ", 2);
	else if (nb == 2)
		ft_putstr_fd("cd: string not in pwd: ", 2);
	if (env && (nb == -1 || nb == 2))
	{
		ft_putstr_fd(arg, 2);
		ft_putchar_fd('\n', 2);
	}
	if (env && nb > 2)
		ft_putstr_fd("cd: too many arguments\n", 2);
}

static int				put_error(char *arg, int nb, t_env **env)
{
	struct stat			info;
	char				*lnk;

	(void)env;
	lnk = (char *)malloc(sizeof(char) * 512);
	lstat(arg, &info);
	readlink(arg, lnk, info.st_size);
	lstat(lnk, &info);
	print_message(info, nb, env, arg);
	free(lnk);
	return (1);
}

static char				*change_path(char *new_rep, char *dir, char *link)
{
	int					size_l;
	int					size_d;
	int					i;

	size_l = ft_strlen(link);
	size_d = ft_strlen(new_rep);
	i = 0;
	while (i++ < size_l)
		size_d--;
	while (*dir)
	{
		new_rep[size_d] = *dir;
		dir++;
		size_d++;
	}
	new_rep[size_d] = '\0';
	return (new_rep);
}

static int				change_dir(char *dir, t_env **env, int opt, int ret)
{
	struct stat			info;
	char				*link;
	char				*new_env;

	new_env = NULL;
	lstat(dir, &info);
	link = (char *)ft_memalloc(sizeof(char) * (ft_strlen(dir) + 1));
	if (S_ISLNK(info.st_mode) != 0)
		readlink(dir, link, info.st_size);
	if (S_ISDIR(info.st_mode) != 0 && access(dir, X_OK) == -1)
	{
		free(link);
		return (error_cd(dir, 3));
	}
	if ((ret = chdir(dir)) == 0)
	{
		new_env = getcwd(new_env, sizeof(new_env));
		if (opt == 0 && S_ISLNK(info.st_mode) != 0)
			new_env = change_path(new_env, dir, link);
		change_env(new_env, env);
		free(new_env);
	}
	free(link);
	return (put_error(dir, ret, env));
}

int						ft_cd(char **arg, t_env **env)
{
	int					opt;

	opt = 0;
	arg++;
	while (*arg && (ft_strcmp(*arg, "-P") == 0 || ft_strcmp(*arg, "-L") == 0))
	{
		if (ft_strcmp(*arg, "-P") == 0)
			opt = 1;
		arg++;
	}
	if (ft_size_tab(arg) > 1)
	{
		write(1, "A\n", 2);
		return (put_error(*arg, ft_size_tab(arg), env));
	}
	if (!(*arg))
		return (change_dir(recovery_content("$HOME", env), env, opt, 0));
	if (ft_strcmp(*arg, "/~") == 0)
		return (put_error("/~", -1, env));
	if (*arg && ft_strcmp(*arg, "-") == 0)
		return (change_dir(recovery_content("$OLDPWD", env), env, opt, 0));
	else
		return (change_dir(*arg, env, opt, 0));
	return (0);
}
