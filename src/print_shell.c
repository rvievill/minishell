/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_shell.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/20 18:03:03 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/30 17:50:45 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"
#include <time.h>

static void		error(char *path)
{
	ft_putstr("I'm lost dumbass");
	ft_putstr(DEF);
	ft_putstr(RED);
	ft_putstr(" ]\n");
	ft_putstr(DEF);
	free(path);
}

static void		ft_path(char *pwd, char *path, char *home)
{
	char		*str;
	char		*str2;
	char		*str3;

	str2 = NULL;
	str = NULL;
	str = getcwd(str, sizeof(str));
	if ((str2 = ft_strstr(pwd, home)) != 0)
	{
		str3 = ft_strjoin(path, str2 + ft_strlen(home));
		ft_putstr(str3);
		free(str3);
	}
	else
	{
		ft_putstr(pwd);
		free(path);
	}
	ft_putstr(DEF);
	ft_putstr(RED);
	ft_putstr(" ]\n");
	ft_putstr(DEF);
	free(str);
}

static void		print_path(t_env *env)
{
	char		*pwd;
	char		*home;
	char		*path;

	path = ft_strdup("~");
	pwd = ft_strdup(recovery_content("$PWD", &env));
	home = ft_strdup(recovery_content("$HOME", &env));
	ft_putstr(RED);
	ft_putstr("[ ");
	ft_putstr(DEF);
	ft_putstr(GREEN);
	ft_putstr(recovery_content("$LOGNAME", &env));
	ft_putstr(": ");
	if (home && pwd)
		ft_path(pwd, path, home);
	else
		error(path);
	free(pwd);
	free(home);
}

static void		print_time(void)
{
	char		*time_cur;
	time_t		t;

	time(&t);
	time_cur = ft_strsub(ctime(&t), 11, 5);
	ft_putstr(RED);
	ft_putstr("[ ");
	ft_putstr(DEF);
	ft_putstr(GREEN);
	ft_putstr(time_cur);
	ft_putstr(DEF);
	ft_putstr(RED);
	ft_putstr(" ] ");
	ft_putstr(DEF);
	free(time_cur);
}

void			print_shell(t_env *env)
{
	(void)env;
	print_time();
	print_path(env);
	ft_putstr("-> ");
}
