/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_exist.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/24 19:46:06 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/25 17:37:30 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

int					is_builtin(char *cmd)
{
	const char		*bui[] =
	{"echo", "cd", "env", "setenv", "unsetenv", "exit", 0};
	int				i;

	i = 0;
	while (cmd && bui[i])
	{
		if (ft_strcmp(cmd, bui[i]) == 0)
			return (0);
		i++;
	}
	return (1);
}

static void			replace_cmd(char **cmd, char *path)
{
	char			*save;
	char			*save_path;

	save_path = ft_strdup(path);
	save = ft_strdup(*cmd);
	if (ft_strchr(*cmd, '/') == NULL)
	{
		free(*cmd);
		*cmd = ft_strjoin(save_path, save);
		save_path = NULL;
	}
	if (save_path)
		free(save_path);
	free(save);
}

static int			search_in_relativ_path(char **cmd)
{
	char			*path;
	char			*exec;
	DIR				*directory;
	struct dirent	*dir;

	exec = ft_strdup(ft_strrchr(*cmd, '/') + 1);
	path = ft_strndup(*cmd, ft_strlen(*cmd) - ft_strlen(exec));
	if ((directory = opendir(path)))
	{
		while ((dir = readdir(directory)))
		{
			if (ft_strcmp(exec, dir->d_name) == 0)
			{
				free(exec);
				free(path);
				closedir(directory);
				return (0);
			}
		}
		closedir(directory);
	}
	free(exec);
	free(path);
	return (1);
}

static int			search_in_path(char *path, char **cmd)
{
	DIR				*directory;
	struct dirent	*dir;
	char			*full_path;
	char			*save_path;

	if ((directory = opendir(path)))
	{
		while ((dir = readdir(directory)))
		{
			save_path = ft_strdup(path);
			full_path = ft_strjoin(save_path, dir->d_name);
			if (ft_strcmp(*cmd, dir->d_name) == 0
					|| ft_strcmp(full_path, *cmd) == 0)
			{
				replace_cmd(cmd, path);
				closedir(directory);
				free(full_path);
				return (0);
			}
			free(full_path);
		}
		closedir(directory);
	}
	return (1);
}

int					is_no_builtin(char **cmd, char **path)
{
	int				i;
	char			**new_tab;

	i = 0;
	if (!cmd)
		return (1);
	if ((*cmd)[0] == '/')
		return (search_in_relativ_path(cmd));
	new_tab = ft_tabcpy(path);
	while (i < ft_size_tab(new_tab))
	{
		new_tab[i] = ft_strcat(new_tab[i], "/");
		if (search_in_path(new_tab[i], cmd) == 0)
		{
			free_tab(new_tab);
			return (0);
		}
		i++;
	}
	free_tab(new_tab);
	return (1);
}
