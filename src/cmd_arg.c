/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_arg.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/30 12:58:32 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/19 15:29:20 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

void			add_cmd(char **line, char *save, int i, t_cmd **cur)
{
	int			j;

	j = 1;
	if (*save == '"')
		dquote(line, &(*cur)->cmd);
	else
	{
		(*cur)->cmd = ft_strndup(*line, i);
		*line = *line + i;
	}
}

static int		count_word(char *line)
{
	int			i;
	int			word;

	i = 0;
	word = 0;
	while (line[i] != '\0' && is_sep(&line[i]) == 0)
	{
		if (line[i] == '"')
		{
			i++;
			while (line[i] != '\0' && line[i] != '"')
				i++;
			word++;
		}
		else if ((line[i + 1] == '\0' || line[i + 1] == ' '
				|| line[i + 1] == '\t' || is_sep(&(line[i + 1])) == 1)
				&& line[i] != ' ' && line[i] != '\t')
			word++;
		i++;
	}
	return (word);
}

static void		arg_whitout_quote(char **line, char **save, char **arg, int j)
{
	char		*tmp;

	tmp = *save;
	while (*tmp && *tmp != ' ' && *tmp != '\t' && is_sep(tmp) == 0)
	{
		tmp++;
		j++;
	}
	*save = tmp;
	*arg = ft_strndup(*line, j);
	*line = *line + j;
}

static void		move_space(char **line, char **save)
{
	char		*tmp;

	tmp = *save;
	while (*tmp && (*tmp == ' ' || *tmp == '\t'))
	{
		tmp++;
		(*line)++;
	}
	*save = tmp;
}

void			add_arg(char **line, t_cmd **cur, int k)
{
	int			size;
	char		*save;
	char		*tmp;
	int			j;

	size = count_word(*line) + 1;
	(*cur)->arg = ft_memalloc(sizeof(char *) * (size + 1));
	(*cur)->arg[0] = ft_strdup((*cur)->cmd);
	save = ft_strdup(*line);
	tmp = save;
	while (++k < size)
	{
		j = 0;
		move_space(line, &save);
		if (*save == '"')
		{
			dquote(line, &(*cur)->arg[k]);
			save = *line;
		}
		else
			arg_whitout_quote(line, &save, &(*cur)->arg[k], j);
	}
	(*cur)->arg[k] = NULL;
	save = tmp;
	free(save);
}
