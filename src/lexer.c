/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 15:06:08 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/25 17:37:54 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

int					is_sep(char *line)
{
	const char		*sep[] = {";", "||", "&&", "\0", 0};
	int				i;

	i = 0;
	while (sep[i] != NULL && sep[i][0] != '\0')
	{
		if (ft_strncmp(sep[i], line, ft_strlen(sep[i])) == 0)
			return (1);
		i++;
	}
	return (0);
}

static int			add_sep(char **line, t_cmd **new)
{
	const char		*sep[] = {";", "||", "&&", "|", "\0", 0};
	int				i;

	i = 0;
	while (sep[i])
	{
		if (sep[i][0] == '\0')
			return (0);
		if (ft_strncmp(sep[i], *line, ft_strlen(sep[i])) == 0)
		{
			(*new)->sep = ft_strdup((char *)sep[i]);
			add_elem(new);
			*line = *line + ft_strlen(sep[i]);
			return (1);
		}
		i++;
	}
	return (0);
}

static void			add_cmd_complet(char **line, t_cmd **cur)
{
	char			*save;
	int				i;
	int				j;

	i = 0;
	j = 0;
	save = *line;
	while (*save && is_sep(save) == 0 && *save != ' ' && *save != '\t'
			&& *save != '"')
	{
		i++;
		save++;
	}
	if ((*cur)->cmd == NULL)
	{
		if (*save == '"')
			while (*save && *save != ' ' && *save != '\t')
			{
				i++;
				save++;
			}
		add_cmd(line, save, i, cur);
	}
	else
		add_arg(line, cur, 0);
}

static void			recovery_home(t_cmd *cmd)
{
	char			*home;
	char			*save;
	int				i;

	home = NULL;
	save = NULL;
	while (cmd)
	{
		if (home == NULL)
			home = ft_strdup("/nfs/2015/r/rvievill");
		i = -1;
		while (cmd->arg && cmd->arg[++i] && cmd->arg[i] != 0)
		{
			if (cmd->arg[i] && cmd->arg[i][0] == '~')
			{
				save = ft_strdup(&cmd->arg[i][1]);
				free(cmd->arg[i]);
				cmd->arg[i] = ft_strjoin(home, save);
				home = NULL;
				free(save);
			}
		}
		cmd = cmd->next;
	}
	free(home);
}

t_cmd				*lexer(char *line, t_env **env)
{
	t_cmd			*new;
	const char		*el = line + (unsigned long)ft_strlen(line);

	new = (t_cmd *)malloc(sizeof(t_cmd));
	init_node(&new);
	while (line < el)
	{
		while (*line && (*line == ' ' || *line == '\t'))
			line++;
		if (!*line)
			break ;
		if (!add_sep(&line, &new))
			add_cmd_complet(&line, &new);
	}
	up_list(&new);
	check_arg(new);
	check_var(new, *env);
	exec_spe(new);
	recovery_home(new);
	up_list(&new);
	return (new);
}
