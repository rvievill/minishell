/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/21 16:55:56 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/20 18:20:37 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static int		put_error_parse(char *line, int word)
{
	if (word == 1 || (*line == ';' && *(line + 1) == ';'))
	{
		ft_putstr_fd("MyShell: parse error near ", 2);
		ft_putchar_fd('`', 2);
		ft_putchar_fd(*line, 2);
		ft_putchar_fd(*(line + 1), 2);
		ft_putchar_fd('\'', 2);
		ft_putchar_fd('\n', 2);
		return (1);
	}
	return (0);
}

static int		parser_error(char *line)
{
	int			word;
	int			ret;

	word = 1;
	ret = 0;
	while (*line)
	{
		while (*line && (*line == ' ' || *line == '\t'))
			line++;
		if ((ret = is_sep(line)) == 1)
		{
			if (put_error_parse(line, word) == 1)
				return (1);
			(*line == ';') ? (line++) : (line = line + 2);
			word = 1;
		}
		else
			word = 0;
		if (ret == 0)
			line++;
	}
	return (0);
}

void			minishell(t_env **env)
{
	char		*line;
	t_cmd		*cmd;
	char		**path;

	line = NULL;
	cmd = NULL;
	while (1)
	{
		print_shell(*env);
		get_next_line(0, &line);
		path = ft_strsplit(recovery_content("$PATH", env), ':');
		if (parser_error(line) == 0 && *line != '\0')
		{
			cmd = lexer(line, env);
			parser(&cmd, env, path);
		}
		free(line);
		free_tab(path);
	}
	free_tab(path);
}
