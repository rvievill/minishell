/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 17:18:19 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/25 17:52:47 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static int		error(char **arg)
{
	if (!arg || !arg[1] || !arg[2] || !arg[3])
	{
		ft_putstr_fd("MyShell: setenv: missing argument\n", 2);
		return (1);
	}
	else if (arg[4])
	{
		ft_putstr_fd("MyShell: setenv: too much argument\n", 2);
		return (1);
	}
	else if (ft_strchr(arg[1], '='))
	{
		ft_putstr_fd("MyShell: setenv: no sign '=' for the variable\n", 2);
		return (1);
	}
	return (0);
}

static void		new_elem(t_env **env, char **arg)
{
	t_env		*new;

	if (!(new = (t_env *)malloc(sizeof(t_env))))
		return ;
	new->var = ft_strdup(arg[1]);
	new->var = ft_strjoin(new->var, "=");
	new->content = ft_strdup(arg[2]);
	while ((*env)->next)
		*env = (*env)->next;
	new->prev = *env;
	(*env)->next = new;
	(*env)->next->next = NULL;
}

static int		exist(t_env **env, char **arg)
{
	if (env && ft_strncmp((*env)->var, arg[1], ft_strlen((*env)->var) - 1) == 0)
	{
		if (ft_strcmp(arg[3], "1") == 0)
		{
			free((*env)->content);
			(*env)->content = ft_strdup(arg[2]);
			while ((*env)->prev)
				*env = (*env)->prev;
			return (1);
		}
		else
		{
			while ((*env)->prev)
				*env = (*env)->prev;
			return (1);
		}
	}
	return (0);
}

int				ft_setenv(char **arg, t_env **env)
{
	if (error(arg) == 1)
		return (1);
	arg[1] = ft_strupcase(arg[1]);
	while (arg[1] && env && (*env)->next)
	{
		if (exist(env, arg) == 1)
			return (0);
		*env = (*env)->next;
	}
	if (exist(env, arg) == 1)
		return (0);
	else if (env)
		new_elem(env, arg);
	while (env && (*env)->prev)
		*env = (*env)->prev;
	return (0);
}
