/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_cd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 13:42:50 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/25 13:51:25 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

int			error_cd(char *dir, int nb)
{
	if (nb == 3)
		ft_putstr_fd("cd: permission denied: ", 2);
	ft_putendl_fd(dir, 2);
	return (1);
}
