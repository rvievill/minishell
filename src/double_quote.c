/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   double_quote.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/29 18:23:20 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/19 15:29:40 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

int					is_quote(char *line)
{
	if (*line == '"')
		return (1);
	return (0);
}

void				dquote(char **line, char **cmd)
{
	char			*save;
	int				i;

	i = 1;
	save = ft_strdup(*line);
	while (save[i] && save[i] != '"')
		i++;
	*cmd = ft_strndup(save + 1, i - 1);
	*line = *line + i + 1;
	free(save);
}
