/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 16:57:04 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/22 15:30:52 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

void			path_rescue(char ***env)
{
	char		*path;
	char		*pwd;
	char		*name;

	name = ft_strdup("PWD=");
	pwd = NULL;
	pwd = getcwd(pwd, sizeof(pwd));
	path = "PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/local/munki";
	if (!(*env = (char **)malloc(sizeof(char *) * 13)))
		return ;
	(*env)[0] = ft_strdup("HOME=/nfs/2015/r/rvievill");
	(*env)[1] = ft_strdup("LOGNAME=rvievill");
	(*env)[2] = ft_strdup("SHLVL=1");
	(*env)[3] = ft_strjoin(name, pwd);
	(*env)[4] = ft_strdup("OLDPWD=/nfs/2015/r/rvievill");
	(*env)[5] = ft_strdup("ZSH=/nfs/2015/r/rvievill/.oh-my-zsh");
	(*env)[6] = ft_strdup(path);
	(*env)[7] = ft_strdup("PAGER=less");
	(*env)[8] = ft_strdup("LESS=-R");
	(*env)[9] = ft_strdup("LC_CTYPE=");
	(*env)[10] = ft_strdup("LSCOLORS=Gxfxcxdxbxegedabagacad");
	(*env)[11] = ft_strdup("_=/usr/bin/env");
	(*env)[12] = 0;
	free(pwd);
}

int				main(int ac, char **av, char **envp)
{
	t_env		*env;
	char		**envp_cpy;

	(void)av;
	if (ac == 1)
	{
		env = NULL;
		envp_cpy = NULL;
		if (envp[0] == NULL)
			path_rescue(&envp_cpy);
		else
			envp_cpy = ft_tabcpy(envp);
		create_lst(&env, envp_cpy);
		minishell(&env);
		free_env(&env);
		free_tab(envp_cpy);
	}
	else
		ft_putstr_fd("too many arguments\n", 2);
	return (0);
}
