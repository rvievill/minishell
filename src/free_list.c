/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_list.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/08 20:08:42 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/19 15:30:18 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

void			free_node(t_cmd *start)
{
	free(start->cmd);
	free_tab(start->arg);
	free(start->sep);
	free(start);
}

void			free_cmd(t_cmd **start)
{
	if (*start)
	{
		free_cmd(&(*start)->next);
		free_node(*start);
	}
}

void			free_env(t_env **start)
{
	if (*start)
	{
		free_env(&(*start)->next);
		ft_memdel((void**)&(*start)->var);
		if ((*start)->content)
			ft_memdel((void**)&(*start)->content);
		free(*start);
	}
}
