/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/25 15:10:58 by rvievill          #+#    #+#             */
/*   Updated: 2016/08/22 10:49:35 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static void		change_str(char *str)
{
	int			i;
	int			j;
	char		*save;

	i = 0;
	j = 0;
	save = ft_strdup(str);
	while (i < (int)ft_strlen(save))
	{
		if (save[i] == '"')
			i++;
		str[j++] = save[i++];
	}
	str[j] = '\0';
	free(save);
}

static int		fill_arg(t_cmd *tmp)
{
	if (!(tmp->arg = (char **)ft_memalloc(sizeof(char *) * 2)))
		return (1);
	tmp->arg[0] = ft_strdup(tmp->cmd);
	tmp->arg[1] = NULL;
	return (0);
}

void			check_arg(t_cmd *cmd)
{
	t_cmd		*tmp;
	int			i;

	tmp = cmd;
	while (tmp)
	{
		if (tmp->cmd && ft_strchr(tmp->cmd, '"'))
			change_str(tmp->cmd);
		if (tmp->cmd && tmp->arg == NULL)
		{
			if (fill_arg(tmp) == 1)
				return ;
		}
		else if (tmp->arg)
		{
			i = 0;
			while (tmp->arg[i])
			{
				if (ft_strchr(tmp->arg[i], '"'))
					change_str(tmp->arg[i]);
				i++;
			}
		}
		tmp = tmp->next;
	}
}
